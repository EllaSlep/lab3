def merge_sort(A):
    if len(A) == 1 or len(A) == 0:
        return A
    L, R = A[:len(A) // 2], A[len(A) // 2:]
    merge_sort(L)
    merge_sort(R) 
    n = m = k = 0
    C = [0] * (len(L) + len(R))
    while n < len(L) and m < len(R):
        if L[n] <= R[m]:
            C[k] = L[n]
            n += 1
        else:
            C[k] = R[m]
            m += 1
        k += 1
    while n < len(L):
        C[k] = L[n]
        n += 1
        k += 1
    while m < len(R):
        C[k] = R[m]
        m += 1
        k += 1
    for i in range(len(A)):
        A[i] = C[i]
    return A

with open ('/Users/macbook/Desktop/ИТМО/алгосы/sort.in.txt','r') as f:
    n=int(f.readline())
    a=list(map(int,f.readline().split()))
    merge_sort(a)
a=' '.join(map(str,a))
with open ('/Users/macbook/Desktop/ИТМО/алгосы/sort.out.txt','w') as f1:
    f1.write(a)
#Love, Ella
